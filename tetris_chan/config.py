import os
if os.path.isfile("highscore.txt") is False:
    hiFile = open("highscore.txt", "w+")
    hiFile.write("0")
    highscore = 0
    hiFile.close()
else:
    hiFile = open("highscore.txt", "r")
    highscore = hiFile.read()
    hiFile.close()

spac = 30
cols = 10
rows = 15
Width = spac*cols
Height = spac*rows
speed = 1 #How fast Objects fall in blocks per half second
pufferLeft = 2
pufferRight = 3
pufferDown = 2

grid = [[0 for a in range(cols)] for b in range(rows)]


def resetGrid():
    global grid
    grid = [[0 for a in range(cols)] for b in range(rows)]
    setupBorder()
    
    
def setupBorder():
    global grid, pufferLeft, pufferRight, pufferDown
    for i in range(len(grid)):
        for j in range(pufferLeft):
            grid[i].insert(0, 1)
        for k in range (pufferRight):
            grid[i].append(1)

    for i in range(pufferDown):
        grid.append([1 for j in range(len(grid[1]))])
