#!/usr/bin/python
# -*- coding: utf-8 -*-
import Square as Sq
import SnakyBoi as Snb
import SteppyBoi as Stb
import LongBoi as lb
import OtherSnakyBoi as OSnb
import LShape as Ls
import OtherLShape as OLs
import config as c

puffer = c.pufferLeft
speed = c.speed
spac = c.spac

class Object:
    
    def __init__(self):
        #replace later with initRandom
        pass


    def initRandom(self):
        x = 3
        y = 0
        num = int(random(0, 7))
        if num == 0:
            self.falling = Sq.Square()
            self.falling.create(x, y)
        elif num == 1:
            self.falling = Snb.SnakyBoi()
            self.falling.create(x, y)
        elif num == 2:
            self.falling = Stb.SteppyBoi()
            self.falling.create(x, y)
        elif num == 3:
            self.falling = lb.LongBoi()
            self.falling.create(x, y)
        elif num == 4:
            self.falling = OSnb.OtherSnakyBoi()
            self.falling.create(x, y)
        elif num == 5:
            self.falling = Ls.LShape()
            self.falling.create(x,y)
        elif num == 6:
            self.falling = OLs.OtherLShape()
            self.falling.create(x,y)
            

    def test_create(self, num, x, y):
        if num == 0:
            self.falling = Sq.Square()
            self.falling.create(x,y)
        elif num == 1:
            self.falling = Snb.SnakyBoi()
            self.falling.create(x,y)
        elif num == 2:
            self.falling = Stb.SteppyBoi()
            self.falling.create(x,y)
        elif num == 3:
            self.falling = lb.LongBoi()
            self.falling.create(x, y)
        elif num == 4:
            self.falling = OSnb.OtherSnakyBoi()
            self.falling.create(x, y)
        elif num == 5:
            self.falling = Ls.LShape()
            self.falling.create(x,y)
        elif num == 6:
            self.falling = OLs.OtherLShape()
            self.falling.create(x,y)
            
            
    def show(self):
        fill(255)
        data=self.falling.data
        yPos = 0
        for i in range(len(data)):
            xPos = 0
            for j in range(len(data[i])):
                if data[i][j] == 1:
                    square(self.falling.x*spac+xPos*spac, self.falling.y*spac+yPos*spac, spac-1)
                xPos += 1
            yPos += 1
        
        
    def canwritenext(self):
        a = self.falling.y+1
        b = self.falling.x
        for y in range(len(self.falling.data)):
            for x in range(len(self.falling.data[0])):
                if c.grid[a+y][b+x+puffer] + self.falling.data[y][x] > 1:
                    return False
        return True
        
    
    def update(self):
        if self.canwritenext() is True:
            self.falling.y += speed
            return True
        else:
            self.write()
            return False
    
    
    def canwriteSides(self, where):
        if where == "r":
            b = self.falling.x+1
        elif where == "l":
            b = self.falling.x-1
        a = self.falling.y
        for y in range(len(self.falling.data)):
            for x in range(len(self.falling.data[0])):
                if c.grid[a+y][b+x+puffer] + self.falling.data[y][x] > 1:
                    return False
        return True

        
    def moveRight(self):
        if self.canwriteSides("r") is True:
            self.falling.x += 1


    def moveLeft(self):
        if self.canwriteSides("l") is True:
            self.falling.x -= 1
            
            
    def rot(self):
        safe = list(self.falling.data)
        self.falling.rot()
        for y in range(4):
            for x in range(4):
                if c.grid[self.falling.y+y][self.falling.x+x+puffer] + self.falling.data[y][x] > 1:
                    self.falling.data = list(safe)
                    return

        
            
    def stop(self):
        if self.bottom() or writeError():
            self.write()
            
            
    def write(self):
        a = self.falling.y
        b = self.falling.x
        for y in range(len(self.falling.data)):
            for x in range(len(self.falling.data[0])):
                c.grid[a+y][b+x+puffer] += self.falling.data[y][x]
