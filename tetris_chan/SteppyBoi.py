#!/usr/bin/python
# -*- coding: utf-8 -*-
import config as c
spac = c.spac
gdata = [[0, 1, 0,0], [1, 1, 1,0], [0, 0, 0,0],[0,0,0,0]]

class SteppyBoi:
    
    def create(self, x, y):
        fill(255)
        self.x = x
        self.y = y
        self.data = list(gdata)
        data = self.data
        yPos = 0
        for i in range(len(data)):
            xPos = 0
            for j in range(len(data[i])):
                if data[i][j] == 1:
                    square(self.x*spac+xPos*spac, self.y*spac+yPos*spac, spac-1)
                xPos += 1
            yPos += 1
    
    
    def rot(self):
        data = self.data
        newData = [[0, 0, 0,0], [0,0,0,0],[0,0,0,0], [0,0,0,0]]
        for row in range(3):
            n = 2
            for col in range(3):
                newData[row][col] = data[n][row]
                n -= 1
        
        self.data = newData
