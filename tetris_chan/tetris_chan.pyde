import config as c
import Object as obj
import time
spac = c.spac
ob = obj.Object()
pl = c.pufferLeft
score = 0
highscore = c.highscore
p = False

def setup():
    size(c.Width, c.Height)
    c.resetGrid()
    #ob.test_create(3, 3, 0)
    #manip()
    ob.initRandom()
    


def draw():
    
    global ob, score
    background(0)
    for y in range(c.rows):
        for x in range(c.cols):
            if c.grid[y][x+pl] == 1:
                fill(255)
            else:
                fill(100)
            square(x * spac, y * spac, spac - 1)
            
            
            """fill(0)
            textSize(10)
            text(c.grid[y][x+pl], x*spac + 0.5*spac, y*spac+0.5*spac)"""
    
    fill(255)
    textSize(12)
    text("Score: " + str(score), 5, 15)

        
    ob.show()
    if frameCount % 30 == 0:
        if ob.update() is False:
            #ob.test_create(6,3,0)
            ob.initRandom()
            delLine()
            gameOver()
            
            
def delLine():
    global score
    l = fullLine()
    if l != -1:
        print("FULL LINE")
        score += 100
        deleteLine(l)

def deleteLine(y):
    grid = c.grid
    for y in range(y, 0, -1):
        for x in range(pl, c.cols+pl):
            if grid[y-1][x] == 1:
                grid[y][x] = 1
            else:
                grid[y][x] = 0
    delLine()

def fullLine():
    pd = c.pufferDown
    y = c.rows - pd +1
    for y in range(c.rows - pd +1, 0, -1):
        if (0 in c.grid[y]) is False:
            return y
    return -1

def gameOver():
    global score
    pr = c.pufferRight
    lst = []
    for i in range(pl, len(c.grid[0])-pr):
        lst.append(c.grid[0][i])
        
    if (1 in lst) is True:
            
        print("GAME OVER")
        print("Score: "+str(score))
        background(0)
        fill(255)
        textSize(25)
        
        global highscore
        if int(highscore) < score:
            highscore = score
            hiFile = open("highscore.txt", "w")
            hiFile.write("%d" % score)
            hiFile.close()
            text("NEW HIGHSCORE!", width/2 - width/4 - 30, height/5)
            
        text("GAME OVER", width/2 - width/4, height/3)
        text("Score: " + str(score), width/2 - width/4 - 30, height/3 + 30)
        text("Highscore: "+ str(highscore), width/2 - width/4 - 30, height/3 + 60)
        
        score = 0
        noLoop()

def reset():
    c.resetGrid()

def keyPressed():
    global ob, p
    if key == "d" or key == "D":
        ob.moveRight()
    elif key == "a" or key == "A":
        ob.moveLeft()
    elif key == "w" or key == "W":
        ob.rot()
    elif key == "s" or key == "S":
        if ob.canwritenext() is True:
            ob.update()
    elif key == "g" or key == "G":
        ob.write()
    elif key == "r" or key == "R":
        print("reset")
        reset()
        loop()
        redraw()
    elif key == "p" or key == "P":
        if p is False:
            p = True
            noLoop()
        else:
            p = False
            loop()
